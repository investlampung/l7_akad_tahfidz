@extends('layouts.admin')
@section('content')

<section class="content-header">
    <h1>
        History
    </h1>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Data History</h3>
                </div>

                <div class="box-body">
                    <div class="box-body" style="overflow-x:auto;">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>User</th>
                                    <th>Aktifitas</th>
                                    <th>Keterangan</th>
                                    <th>Created At</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach( $data as $item )
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $item->user }}</td>
                                    @if($item->info=="Tambah")
                                    <td><span class="badge bg-blue">{{ $item->info}}</span></td>
                                    @elseif($item->info=="Ubah")
                                    <td><span class="badge bg-green">{{ $item->info }}</span></td>
                                    @else
                                    <td><span class="badge bg-red">{{ $item->info }}</span></td>
                                    @endif
                                    <td>{{ $item->desc_info }}</td>
                                    <td>{{ $item->created_at }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection