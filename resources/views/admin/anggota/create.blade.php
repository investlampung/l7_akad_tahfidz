@extends('layouts.admin')

@section('content')
<div class="container">

    <section class="content-header">
        <h1>
            Anggota
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Tambah Data Anggota</h3>
                </div>

                <div class="box-body" style="overflow-x:auto;">
                    <form action="{{ route('admin.anggota.store') }}" class="form-horizontal" method="POST">
                        @csrf

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Nama</label>

                                <div class="col-sm-8">
                                    <input type="text" name="nama" class="form-control" placeholder="Nama">
                                    <small class="text-danger">{{ $errors->first('nama') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">NIK</label>

                                <div class="col-sm-8">
                                    <input type="number" name="no_ktp" class="form-control" placeholder="NIK">
                                    <small class="text-danger">{{ $errors->first('no_ktp') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Tempat Lahir</label>

                                <div class="col-sm-8">
                                    <input type="text" name="tempat_lahir" class="form-control" placeholder="Tempat Lahir">
                                    <small class="text-danger">{{ $errors->first('tempat_lahir') }}</small>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Tanggal Lahir</label>

                                <div class="col-sm-8">
                                    <input type="date" name="tanggal_lahir" class="form-control">
                                    <small class="text-danger">{{ $errors->first('tanggal_lahir') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Jenis Kelamin</label>

                                <div class="col-sm-8">
                                    <select class="form-control" name="jk">
                                        <option value="Perempuan">Perempuan</option>
                                        <option value="Laki - Laki">Laki - Laki</option>
                                    </select>
                                    <small class="text-danger">{{ $errors->first('jk') }}</small>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Alamat</label>

                                <div class="col-sm-8">
                                    <input type="text" name="alamat" class="form-control" placeholder="Alamat">
                                    <small class="text-danger">{{ $errors->first('alamat') }}</small>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">No Telp</label>

                                <div class="col-sm-8">
                                    <input type="text" name="no_telp" class="form-control" placeholder="No Telp">
                                    <small class="text-danger">{{ $errors->first('no_telp') }}</small>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Pekerjaan</label>

                                <div class="col-sm-8">
                                    <select class="select2 form-control" name="pekerjaan_id">
                                        @foreach($pekerjaan as $pek)
                                        <option value="{{$pek->id}}">{{$pek->pekerjaan}}</option>
                                        @endforeach
                                    </select>
                                    <small class="text-danger">{{ $errors->first('pekerjaan_id') }}</small>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Blok Kaveling</label>

                                <div class="col-sm-8">
                                    <input type="text" name="no_kapling" class="form-control" placeholder="No Kapling">
                                    <small class="text-danger">{{ $errors->first('no_kapling') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Sistem Bayar</label>

                                <div class="col-sm-8">
                                    <select class="form-control" name="sistem_bayar">
                                        <option value="Cash">Cash</option>
                                        <option value="DP">DP</option>
                                        <option value="Kredit">Kredit</option>
                                    </select>
                                    <small class="text-danger">{{ $errors->first('sistem_bayar') }}</small>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Dana</label>

                                <div class="col-sm-8">
                                    <input type="number" name="dana" class="form-control" placeholder="Dana">
                                    <small class="text-danger">{{ $errors->first('dana') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Leader</label>

                                <div class="col-sm-8">
                                    <input type="text" name="leader" class="form-control" placeholder="Leader">
                                    <small class="text-danger">{{ $errors->first('leader') }}</small>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Tanam Pohon</label>

                                <div class="col-sm-8">
                                    <select class="form-control" name="tanam_pohon">
                                        <option value="Tidak Setuju">Tidak Setuju</option>
                                        <option value="Setuju">Setuju</option>
                                    </select>
                                    <small class="text-danger">{{ $errors->first('tanam_pohon') }}</small>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Jadi SHM</label>

                                <div class="col-sm-8">
                                    <select class="form-control" name="jadi_shm">
                                        <option value="Tidak Setuju">Tidak Setuju</option>
                                        <option value="Setuju">Setuju</option>
                                    </select>
                                    <small class="text-danger">{{ $errors->first('jadi_shm') }}</small>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Keterangan</label>

                                <div class="col-sm-8">
                                    <input type="text" name="ket" class="form-control" placeholder="Keterangan">
                                    <small class="text-danger">{{ $errors->first('ket') }}</small>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                            <a href="{{url('admin/anggota')}}" class="btn btn-default  pull-right">Kembali</a>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('.select2').select2({
        ajax: {
            url: '{{url('admin-anggota-ajax')}}',
            dataType: 'json',
            delay: 250,
            processResults: function(data) {
                return {
                    results: $.map(data, function(item) {
                        return {
                            text: item.pekerjaan,
                            id: item.id
                        }
                    })
                };
            },
            cache: true
        }
    });
</script>
@endsection