@extends('layouts.admin')

@section('content')

<div class="container">

    <section class="content-header">
        <h1>
            Anggota
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-12">
            <div class="col-md-2">
                <a href="{{ route('admin.anggota.create') }}" class="btn btn-primary">+ Anggota</a>
            </div>
            <div class="col-md-3">
                <a class="btn btn-success" href="{{ route('export') }}">Download Data Anggota</a><br><br>
            </div>
        </div>
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Data Anggota</h3>
                </div>
                <div class="box-body" style="overflow-x:auto;">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Anggota ID</th>
                                <th>No Kapling</th>
                                <th>Nama</th>
                                <th>Leader</th>
                                <th>Sistem Bayar</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($data as $item)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $item->id }}</td>
                                <td>{{ $item->no_kapling }}</td>
                                <td>{{ $item->nama }}</td>
                                <td>{{ $item->leader }}</td>
                                <td>{{ $item->sistem_bayar }}</td>
                                <td align="center">
                                    <form action="{{ route('admin.anggota.destroy',$item->id) }}" method="POST">
                                        <a class="btn btn-primary" href="#" data-toggle="modal" data-target="#modal-barang{{$item->id }}">Lihat</a>
                                        <a class="btn btn-success" href="{{ route('admin.anggota.edit',$item->id) }}">Ubah</a>

                                        @csrf
                                        @method('DELETE')

                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                    </form>
                                </td>
                            </tr>
                            <!-- Anggota -->
                            <div class="modal fade in" id="modal-barang{{$item->id}}">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span></button>
                                            <h4 class="modal-title">
                                                Data {{$item->nama}}
                                            </h4>
                                        </div>

                                        <div class="box-body">
                                            <div class="col-md-4">
                                                NIK
                                            </div>
                                            <div class="col-md-8">
                                                : {{ $item->no_ktp }}
                                            </div>
                                        </div>
                                        <div class="box-body">
                                            <div class="col-md-4">
                                                Tempat Tanggal Lahir
                                            </div>
                                            <div class="col-md-8">
                                                @if(empty($item->tanggal_lahir))
                                                : {{ $item->tempat_lahir }},
                                                @else
                                                : {{ $item->tempat_lahir }}, {{ tanggal_local($item->tanggal_lahir) }}
                                                @endif
                                            </div>
                                        </div>
                                        <div class="box-body">
                                            <div class="col-md-4">
                                                Jenis Kelamin
                                            </div>
                                            <div class="col-md-8">
                                                : {{ $item->jk }}
                                            </div>
                                        </div>
                                        <div class="box-body">
                                            <div class="col-md-4">
                                                Alamat
                                            </div>
                                            <div class="col-md-8">
                                                : {{ $item->alamat }}
                                            </div>
                                        </div>
                                        <div class="box-body">
                                            <div class="col-md-4">
                                                No Telp
                                            </div>
                                            <div class="col-md-8">
                                                : {{ $item->no_telp }}
                                            </div>
                                        </div>
                                        <div class="box-body">
                                            <div class="col-md-4">
                                                Pekerjaan
                                            </div>
                                            <div class="col-md-8">
                                                : {{ $item->pekerjaan->pekerjaan }}
                                            </div>
                                        </div>
                                        <div class="box-body">
                                            <div class="col-md-4">
                                                Tanam Pohon
                                            </div>
                                            <div class="col-md-8">
                                                : {{ $item->tanam_pohon }}
                                            </div>
                                        </div>
                                        <div class="box-body">
                                            <div class="col-md-4">
                                                Kepemilikan Jadi SHM
                                            </div>
                                            <div class="col-md-8">
                                                : {{ $item->jadi_shm }}
                                            </div>
                                        </div>
                                        <div class="box-body">
                                            <div class="col-md-4">
                                                Keterangan
                                            </div>
                                            <div class="col-md-8">
                                                : {{ $item->ket }}
                                            </div>
                                        </div>
                                        <div class="box-footer">
                                            <button class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection