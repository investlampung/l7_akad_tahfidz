<table width="100%">
    <tbody>
        <tr>
            <td valign="top" width="20px">4.</td>
            <td colspan="3" class="kanankiri">
                <b>PENGELOLA</b> tidak bertanggung jawab terhadap pembiayaan surat Sertifikat Hak Milik yang dibeli dengan Pembiayaan Murabahah.
            </td>
        </tr>
        <tr>
            <td class="tengah" colspan="4">
                <b>
                    <br>PASAL 15<br>
                    PENAGIHAN SEKETIKA SELURUH HUTANG MURABAHAH<br><br>
                </b>
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">1.</td>
            <td colspan="3" class="kanankiri">
                Menyimpang dari jangka waktu Pembiayaan, <b>PENGELOLA</b> berhak mengakhiri jangka waktu Pembiayaan dan menagih pelunasan sekaligus atas seluruh sisa Hutang dan <b>PEMBELI</b> wajib membayar dengan seketika dan sekaligus melunasi sisa Hutang yang ditagih oleh <b>PENGELOLA</b> atau melakukan upaya-upaya hukum lain untuk menyelesaikan Pembiayaan, bila <b>PEMBELI</b> ternyata tidak memenuhi kewajibannya yaitu:
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px">a.</td>
            <td colspan="3" class="kanankiri">
                <b>PEMBELI</b> wanprestasi.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px">b.</td>
            <td colspan="3" class="kanankiri">
                <b>PEMBELI</b> diperkirakan tidak akan mampu lagi untuk memenuhi sesuatu ketentuan atau kewajiban di dalam Akad ini, karena terjadinya antara lain peristiwa sebagai berikut:
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px">(1)</td>
            <td colspan="2" class="kanankiri">
                <b>PEMBELI</b> diberhentikan dari Kantor/Instansi yang bersangkutan, dijatuhi hukuman Pidana, mendapat cacat badan, sehingga oleh karenanya belum/tidak dapat dipekerjakan lagi atau
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px">(2)</td>
            <td colspan="2" class="kanankiri">
                <b>PEMBELI</b> telah dinyatakan pailit atau tidak mampu membayar atau telah dikeluarkan perintah oleh pejabat yang berwenang untuk menunjuk wakil atau kuratornya.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px">c.</td>
            <td colspan="2" class="kanankiri">
                Barang dipergunakan untuk hal-hal yang melanggar prinsip Syariah.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px">d.</td>
            <td colspan="2" class="kanankiri">
                <b>PEMBELI</b> membuat atau menyebabkan atau menyetujui dilakukan atau membiarkan dilakukan suatu tindakan yang membahayakan atau dapat membahayakan, mengurangi nilai atau meniadakan jaminan atas Pembiayaan yang telah diterima.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px">e.</td>
            <td colspan="2" class="kanankiri">
                Barang yang diberikan oleh <b>PEMBELI</b> sebagai jaminan Pembiayaan dialihkan pihak lain.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px">f.</td>
            <td colspan="2" class="kanankiri">
                Barang dipergunakan untuk hal-hal yang melanggar prinsip Syariah.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px">g.</td>
            <td colspan="2" class="kanankiri">
                <b>PEMBELI</b> tidak atau lalai memperpanjang jangka waktu hak atas tanah/barang yang dijaminkan kepada <b>PENGELOLA</b>, sesuai dengan ketentuan yang berlaku sebelum jangka waktu hak tersebut habis.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px">h.</td>
            <td colspan="2" class="kanankiri">
                Keterangan yang diberikan atau hal-hal yang disampaikan atau bukti kepemilikan atas jaminan yang diserahkan kepada <b>PENGELOLA</b> terbukti palsu atau <b>PEMBELI</b> lalai atau gagal untuk memberikan keterangan yang sesungguhnya kepada <b>PENGELOLA</b>.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px">i.</td>
            <td colspan="2" class="kanankiri">
                <b>PEMBELI</b> bertindak bertentangan dengan suatu peraturan perundang- undangan yang berlaku yang mempunyai akibat penting terhadap atau mempengaruhi hubungan kerjanya dengan kantor tempat bekerja.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px">j.</td>
            <td colspan="2" class="kanankiri">
                Setiap sebab atau kejadian apapun antara lain perubahan bidang moneter, keuangan atau politik nasional yang mempengaruhi kegiatan bisnis pada umumnya dan menurut pertimbangan bisnis <b>PENGELOLA</b> tidak mungkin lagi meneruskan fasilitas Pembiayaan yang diberikan baik sementara maupun untuk seterusnya, sehingga menjadi layak bagi <b>PENGELOLA</b> untuk melakukan penagihan seketika seluruh sisa Hutang guna melindungi kepentingan-kepentingannya.
            <br><br><br><br>
            </td>
        </tr>
    </tbody>
</table>