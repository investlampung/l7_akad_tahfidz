<table width="100%">
    <tbody>
        <tr>
            <td width="20px" valign="top">2.</td>
            <td colspan="2" class="kanankiri">
                Realisasi Pembiayaan akan dilakukan oleh <b>PENGELOLA</b> kepada Pemasok, baik secara langsung maupun melalui <b>PEMBELI</b> .
            </td>
        </tr>
        <tr>
            <td width="20px" valign="top">3.</td>
            <td colspan="2" class="kanankiri">
                Sejak ditandatanganinya Akad ini dan telah diterimanya Barang pesanan oleh <b>PEMBELI</b> , maka risiko atas Barang tersebut sepenuhnya menjadi tanggung jawab <b>PEMBELI</b> dan dengan ini <b>PEMBELI</b> membebaskan <b>PENGELOLA</b> dari segala tuntutan dan atau ganti rugi berupa apapun atas risiko tersebut.
            </td>
        </tr>
        <tr>
            <td width="20px" valign="top">4.</td>
            <td colspan="2" class="kanankiri">
                Apabila <b>PENGELOLA</b> telah membayar kepada Pemasok termasuk pembayaran uang muka, maka <b>PEMBELI</b> tidak dapat membatalkan secara sepihak Akad ini.
            </td>
        </tr>
        <tr>
            <td class="tengah" colspan="3">
                <b>
                    <br>PASAL 5<br>
                    JATUH TEMPO PEMBIAYAAN<br><br>
                </b>
            </td>
        </tr>
        <tr>
            <td colspan="3" class="kanankiri">
                Fasilitas pembiayaan yang dimaksud dalam Akad ini berlangsung untuk jangka waktu <b>{{$data->pasal5_a}}</b> bulan terhitung sejak tanggal Akad ini ditandatangani serta berakhir pada tanggal {{tanggal_local($data->tanggal)}}.
            </td>
        </tr>
        <tr>
            <td colspan="3" class="kanankiri">
                <br>Berakhirnya jatuh tempo Pembiayaan tidak dengan sendirinya menyebabkan Hutang lunas sepanjang masih terdapat sisa Hutang <b>PEMBELI</b> .
            </td>
        </tr>
        <tr>
            <td class="tengah" colspan="3">
                <b>
                    <br>PASAL 6<br>
                    POTONGAN HARGA/DISKON<br><br>
                </b>
            </td>
        </tr>
        <tr>
            <td colspan="3" class="kanankiri">
                Jika <b>PENGELOLA</b> mendapat potongan harga dari pemasok, maka potongan itu merupakan hak <b>PEMBELI</b> , baik terjadi sebelum maupun sesudah akad.
            </td>
        </tr>
        <tr>
            <td class="tengah" colspan="3">
                <b>
                    <br>PASAL 7<br>
                    PEMBAYARAN KEMBALI PEMBIAYAAN<br><br>
                </b>
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">1.</td>
            <td colspan="2" class="kanankiri">
                <b>PEMBELI</b> wajib melakukan pembayaran kembali Pembiayaan secara angsuran sampai dengan seluruh Hutang Murabahah <b>PEMBELI</b> lunas sesuai dengan jadwal angsuran yang disepakati sebagaimana terdapat pada Lampiran [AAA].
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">2.</td>
            <td colspan="2" class="kanankiri">
                Dalam hal jatuh tempo pembayaran angsuran Pembiayaan Murabahah jatuh bertepatan dengan bukan pada Hari Kerja <b>PENGELOLA</b>, maka <b>PEMBELI</b> berjanji dan dengan ini mengikatkan diri untuk melakukan pembayaran pada Hari Kerja <b>PENGELOLA</b> berikutnya kecuali jika jatuh temponya pada akhir bulan berjalan, maka pembayarannya dilakukan pada Hari Kerja <b>PENGELOLA</b> sebelumnya.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">3.</td>
            <td colspan="2" class="kanankiri">
                Setiap pembayaran yang diterima oleh <b>PENGELOLA</b> dari <b>PEMBELI</b> atas kewajiban Pembiayaan dibukukan oleh <b>PENGELOLA</b> kedalam account <b>PEMBELI</b> sesuai dengan kebijakan <b>PENGELOLA</b> berdasarkan catatan dan pembukuan yang ada pada <b>PENGELOLA</b>.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">4.</td>
            <td colspan="2" class="kanankiri">
                Setiap pembayaran oleh <b>PEMBELI</b> kepada <b>PENGELOLA</b> akan digunakan untuk membayar :
            </td>
        </tr>
        <tr>
            <td width="20px"></td>
            <td width="20px">a.</td>
            <td class="kanankiri">
                pertama, melunasi pembayaran angsuran/pelunasan atas Harga Jual;
            </td>
        </tr>
        <tr>
            <td width="20px"></td>
            <td width="20px">b.</td>
            <td class="kanankiri">
                kedua, biaya ganti rugi;
            </td>
        </tr>
        <tr>
            <td width="20px"></td>
            <td width="20px">c.</td>
            <td class="kanankiri">
                ketiga, denda keterlambatan; dan
            </td>
        </tr>
    </tbody>
</table>