<table width="100%">
    <tbody>
        <tr>
            <td class="tengah" colspan="3"><img src="{{asset('itlabil/images/default/bismillah.png')}}" alt="" width="200px"></td>
        </tr>
        <tr>
            <td class="tengah" colspan="3"><i>"Dengan menyebut Nama Allah Yang Maha Pengasih lagi Maha Penyayang"</i></td>
        </tr>
        <tr>
            <td class="tengah" colspan="3">
                <b>
                    PERJANJIAN PENGELOLAAN BIDANG<br>
                    TANAH/KAVELING<br>
                    ANTARA<br>
                    KOPERASI MITRA GRIYA INDONESIA<br>
                    DAN<br>
                    {{$data->anggota->nama}}<br>
                    Nomor {{$data->nomor}}<br><br>
                </b>
            </td>
        </tr>
        <tr>
            <td colspan="3">Yang bertanda tangan dibawah ini:<br></td>
        </tr>
        <tr>
            <td width="15px" valign="top">I.</td>
            <td class="kanankiri" colspan="2">
                Koperasi Mitra Griya Indonesia, berkantor di Jalan satria no 147 Gadingrejo Pringsewu Lampung – Indonesia dalam hal ini melalui,
            </td>
        </tr>
        <tr>
            <td></td>
            <td width="35%">Diwakili oleh</td>
            <td>: Wahyudi</td>
        </tr>
        <tr>
            <td></td>
            <td>Dalam Kapasitasnya selaku</td>
            <td>: Ketua</td>
        </tr>
        <tr>
            <td width="10px"></td>
            <td class="kanankiri" colspan="2">
                Berdasarkan Perjanjian Layanan Pengelolaan lahan Berbasis Teknologi Dengan Prinsip Syariah No. 16
                Tanggal .......................... dalam hal ini bertindak selaku Ketua dari <b>KOPERASI MITRA GRIYA INDONESIA</b>, selanjutnya
                disebut <b>PENGELOLA</b>;<br>
            </td>
        </tr>
        <tr>
            <td>II.</td>
            <td width="30%">Nama</td>
            <td>: {{$data->anggota->nama}}</td>
        </tr>
        <tr>
            <td></td>
            <td>Pekerjaan</td>
            <td>: {{$data->anggota->pekerjaan->pekerjaan}}</td>
        </tr>
        <tr>
            <td></td>
            <td valign="top">Alamat</td>
            <td>: {{$data->anggota->alamat}}</td>
        </tr>
        <tr>
            <td></td>
            <td>Nomor KTP</td>
            <td>: {{$data->anggota->no_ktp}}</td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2">Dalam hal ini bertindak untuk diri sendiri, selanjutnya disebut <b>PEMBELI</b>.<br><br></td>
        </tr>
        <tr>
            <td class="kanankiri" colspan="3">
                Bahwa <b>PEMBELI</b> telah mengajukan permohonan pembelian bidang tanah/kaveling kepada <b>PENGELOLA</b> untuk membeli bidang tanah/kaveling secara kredit (sebagaimana didefinisikan dalam Perjanjian) dan selanjutnya <b>PENGELOLA</b> menyetujui untuk menyediakan bidang tanah/kaveling itu sesuai dengan ketentuan dan syarat-syarat sebagaimana dinyatakan dalam Perjanjian.
                <br><br>
            </td>
        </tr>
        <tr>
            <td class="kanankiri" colspan="3">
                Dengan ini kedua belah pihak telah sepakat untuk mengadakan Perjanjian Pengelolaan & Pembiayaan dengan prinsip Murabahah (selanjutnya disebut “Akad”) berdasarkan ketentuan dan syarat-syarat sebagai berikut:
                <br><br>
            </td>
        </tr>
        <tr>
            <td class="tengah" colspan="3">
                <b>
                    PASAL 1<br>
                    KETENTUAN POKOK AKAD<br>
                </b>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                Ketentuan-ketentuan pokok Akad ini meliputi sebagai berikut:
            </td>
        </tr>
        <tr>
            <td>a.</td>
            <td>Harga Beli</td>
            <td>: Rp. 40.000.000,- (empat puluh juta rupiah)</td>
        </tr>
        <tr>
            <td>b.</td>
            <td>Marjin Keuntungan</td>
            <td>: Rp. 5.000.000,- (lima juta rupiah)</td>
        </tr>
        <tr>
            <td>c.</td>
            <td>Harga Jual</td>
            <td>: Rp. 45.000.000,- (empat puluh lima juta rupiah)</td>
        </tr>
    </tbody>
</table>