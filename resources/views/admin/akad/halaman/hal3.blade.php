<table width="100%">
    <tbody>
        <tr>
            <td valign="top">12.</td>
            <td class="kanankiri" colspan="2">
                <b>Jatuh Tempo Pembayaran Angsuran</b> adalah tanggal <b>PEMBELI</b> berkewajiban membayar angsuran setiap bulan.
            </td>
        </tr>
        <tr>
            <td valign="top">13.</td>
            <td class="kanankiri" colspan="2">
                <b>Jaminan</b> adalah jaminan yang bersifat materiil maupun immaterial untuk mendukung keyakinan <b>PENGELOLA</b> atas kemampuan dan kesanggupan <b>PEMBELI</b> untuk melunasi Hutangnya sesuai Akad.
            </td>
        </tr>
        <tr>
            <td valign="top">14.</td>
            <td class="kanankiri" colspan="2">
                <b>Dokumen Jaminan</b> adalah akta-akta, surat-surat bukti kepemilikan, dan surat lainnya yang merupakan bukti hak atas barang jaminan berikut surat-surat lain yang merupakan satu kesatuan dan bagian tidak terpisah dari barang jaminan guna menjamin pemenuhan kewajiban <b>PEMBELI</b> kepada <b>PENGELOLA</b> berdasarkan Akad ini.
            </td>
        </tr>
        <tr>
            <td class="tengah" colspan="3">
                <b>
                    <br>PASAL 3<br>
                    PELAKSANAAN<br>
                </b>
            </td>
        </tr>
        <tr>
            <td colspan="3" class="kanankiri">
                Pelaksanaan prinsip Murabahah yang berlangsung antara <b>PENGELOLA</b> dengan <b>PEMBELI</b> sebagai Penerima Fasilitas Pembiayaan dilaksanakan dan diatur menurut ketentuan- ketentuan dan persyaratan sebagai berikut :
            </td>
        </tr>
        <tr>
            <td valign="top">1.</td>
            <td colspan="2" class="kanankiri">
                <b>PEMBELI</b> membutuhkan Barang dengan spesifikasi sebagaimana terdapat pada Lampiran [BBB] dan meminta kepada <b>PENGELOLA</b> untuk memberikan fasilitas Pembiayaan Murabahah guna pembelian Barang.
            </td>
        </tr>
        <tr>
            <td valign="top">2.</td>
            <td colspan="2" class="kanankiri">
                <b>PENGELOLA</b> bersedia menyediakan Pembiayaan Murabahah sesuai dengan permohonan <b>PEMBELI</b>.
            </td>
        </tr>
        <tr>
            <td width="20px" valign="top">3.</td>
            <td colspan="2" class="kanankiri">
                <b>PEMBELI</b> bersedia membayar Harga Jual Barang sesuai Akad ini, dan Harga Jual tidak dapat berubah selama berlakunya Akad ini.
            </td>
        </tr>
        <tr>
            <td width="20px" valign="top">4.</td>
            <td colspan="2" class="kanankiri">
                <b>PENGELOLA</b> dengan Akad ini mewakilkan secara penuh kepada <b>PEMBELI</b> untuk membeli dan menerima Barang dari Pemasok, serta memberi hak melakukan pembuatan akta jual beli untuk dan atas nama <b>PEMBELI</b> sendiri langsung dengan Pemasok.
            </td>
        </tr>
        <tr>
            <td width="20px" valign="top">5.</td>
            <td colspan="2" class="kanankiri">
                Pemberian kuasa sebagaimana dimaksud dalam ayat 4 pasal ini, tidak mengakibatkan <b>PEMBELI</b> dapat membatalkan jual beli Barang serta <b>PEMBELI</b> tidak dapat menuntut <b>PENGELOLA</b> untuk memberikan ganti rugi sebagaimana dimaksud dalam pasal 1471 Kitab Undang-Undang Hukum Perdata.
            </td>
        </tr>
        <tr>
            <td class="tengah" colspan="3">
                <b>
                    <br>PASAL 4<br>
                    SYARAT REALISASI PEMBIAYAAN<br><br>
                </b>
            </td>
        </tr>
        <tr>
            <td width="20px" valign="top">1.</td>
            <td colspan="2" class="kanankiri">
                <b>PENGELOLA</b> akan merealisasikan Pembiayaan berdasarkan Akad ini, setelah <b>PEMBELI</b> terlebih dahulu memenuhi seluruh persyaratan sebagai berikut:
            </td>
        </tr>
        <tr>
            <td width="20px" valign="top"></td>
            <td width="20px" valign="top">a.</td>
            <td class="kanankiri">
                Menyerahkan kepada <b>PENGELOLA</b> seluruh dokumen yang disyaratkan oleh <b>PENGELOLA</b> termasuk tetapi tidak terbatas pada dokumen bukti diri <b>PEMBELI</b> , dokumen kepemilikan jaminan dan atau surat lainnya yang berkaitan dengan Akad ini dan pengikatan jaminan, yang ditentukan dalam Surat Penawaran Pembiayaan dari <b>PENGELOLA</b>.
            </td>
        </tr>
        <tr>
            <td width="20px" valign="top"></td>
            <td width="20px" valign="top">b.</td>
            <td class="kanankiri">
                <b>PEMBELI</b> wajib membuka dan memelihara akun pada <b>PENGELOLA</b> selama <b>PEMBELI</b> mempunyai Pembiayaan Murabahah dari <b>PENGELOLA</b>.
            </td>
        </tr>
        <tr>
            <td width="20px" valign="top"></td>
            <td width="20px" valign="top">c.</td>
            <td class="kanankiri">
                Menandatangani Akad ini dan perjanjian pengikatan jaminan yang disyaratkan oleh <b>PENGELOLA</b>.
            </td>
        </tr>
        <tr>
            <td width="20px" valign="top"></td>
            <td width="20px" valign="top">d.</td>
            <td class="kanankiri">
                Menyetorkan uang muka pembelian dan atau biaya-biaya yang disyaratkan oleh <b>PENGELOLA</b> sebagai yang tercantum dalam Surat Penawaran Pembiayaan.
            </td>
        </tr>
    </tbody>
</table>