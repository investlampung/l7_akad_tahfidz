<table width="100%">
    <tbody>
        <tr>
            <td class="tengah" colspan="3">
                <b>
                    LAMPIRAN<br>
                    JADWAL PEMBAYARAN ANGSURAN<br><br>
                </b>
            </td>
        </tr>
        <tr>
            <td width="30%">Harga Beli</td>
            <td>: Rp. 40.000.000,- (empat puluh juta rupiah)</td>
        </tr>
        <tr>
            <td>Marjin Keuntungan</td>
            <td>: Rp. 5.000.000,- (lima juta rupiah)</td>
        </tr>
        <tr>
            <td>Harga Jual</td>
            <td>: Rp. 45.000.000,- (empat puluh lima juta rupiah)</td>
        </tr>
        <tr>
            <td>Angsuran per bulan</td>
            @php
            $kredhit = terbilang($data->anggota->dana);
            @endphp
            @if($data->anggota->sistem_bayar==='Kredit')
            @if(empty($data->anggota->dana))
            <td>: Rp. </td>
            @else
            <td>: Rp. {{number_format($data->anggota->dana, 0, ".", ".") }},- ({{strtolower($kredhit)}} rupiah)</td>
            @endif
            @else
            <td>: Rp. </td>
            @endif
        </tr>
    </tbody>
</table>
<table class="table-a" width="100%" style="margin-top: 30px;">
    <thead>
        <tr class="tr">
            <th class="th">Tanggal Pembayaran</th>
            <th class="th">Jumlah Pembayaran Angsuran</th>
        </tr>
    </thead>
    <tbody>
        <tr height="25px" class="tr">
            <td class="td"></td>
            <td class="td"></td>
        </tr>
        <tr height="25px" class="tr">
            <td class="td"></td>
            <td class="td"></td>
        </tr>
        <tr height="25px" class="tr">
            <td class="td"></td>
            <td class="td"></td>
        </tr>
        <tr height="25px" class="tr">
            <td class="td"></td>
            <td class="td"></td>
        </tr>
        <tr height="25px" class="tr">
            <td class="td"></td>
            <td class="td"></td>
        </tr>
        <tr height="25px" class="tr">
            <td class="td"></td>
            <td class="td"></td>
        </tr>
        <tr height="25px" class="tr">
            <td class="td"></td>
            <td class="td"></td>
        </tr>
        <tr height="25px" class="tr">
            <td class="td"></td>
            <td class="td"></td>
        </tr>
        <tr height="25px" class="tr">
            <td class="td"></td>
            <td class="td"></td>
        </tr>
        <tr height="25px" class="tr">
            <td class="td"></td>
            <td class="td"></td>
        </tr>
        <tr height="25px" class="tr">
            <td class="td"></td>
            <td class="td"></td>
        </tr>
        <tr height="25px" class="tr">
            <td class="td"></td>
            <td class="td"></td>
        </tr>
    </tbody>
</table>
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>