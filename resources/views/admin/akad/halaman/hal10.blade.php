<table width="100%">
    <tbody>
        <tr>
            <td class="tengah" colspan="3">
                <b>
                    <br>PASAL 21<br>
                    PENUTUP<br>
                </b>
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">1.</td>
            <td colspan="2" class="kanankiri">
                Uraian pasal demi pasal Akad ini, telah dibaca, dimengerti dan dipahami serta disetujui oleh <b>PEMBELI</b> dan <b>PENGELOLA</b>.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">2.</td>
            <td colspan="2" class="kanankiri">
                Segala sesuatu yang belum diatur atau perubahan dalam Akad ini akan di atur dalam surat- menyurat berdasarkan kesepakatan bersama antara <b>PENGELOLA</b> dan <b>PEMBELI</b> yang merupakan bagian yang tidak terpisahkan dari Akad ini.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">3.</td>
            <td colspan="2" class="kanankiri">
                Akad ini mulai berlaku sejak tanggal ditandatanganinya.
            </td>
        </tr>
    </tbody>
</table>
<table width="100%">
    <tbody>
        <tr>
            <td width="30%"></td>
            <td></td>
            <td align="center" width="40%"><br>GADINGREJO,
                @if(empty($data->tanggal))
                ........
                @else
                {{ tanggal_local($data->tanggal) }}
                @endif
                <br><br></td>
        </tr>
        <tr>
            <td valign="top" align="center"><b><b>PEMBELI</b></b></td>
            <td></td>
            <td align="center"><b><b>PENGELOLA</b><br>KOPERASI MITRA GRIYA INDONESIA</b></td>
        </tr>
        <tr>
            <td align="center"><br><br><br><br><br><br><br><b>( {{$data->anggota->nama}} )</b></td>
            <td></td>
            <td align="center">
                <img src="{{asset('itlabil/images/default/ttd.png')}}" alt="" width="150px"><br>
                <b>( WAHYUDI )</b>
            </td>
        </tr>
    </tbody>
</table>
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>