<table width="100%">
    <tbody>
        <tr>
            <td width="20px"></td>
            <td width="20px">d.</td>
            <td class="kanankiri">
                keempat, biaya-biaya lain.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">5.</td>
            <td colspan="2" class="kanankiri">
                Dalam hal <b>PEMBELI</b> merasa bahwa pembukuan/pencatatan <b>PENGELOLA</b> atas kewajiban dan pembayaran yang telah dilakukan tidak benar, <b>PEMBELI</b> berhak untuk mengajukan keberatan/ klaim kepada <b>PENGELOLA</b> dengan disertai bukti-bukti pembayaran yang sah. Namun bila <b>PEMBELI</b> tidak dapat menunjukkan bukti-bukti pembayaran yang sah, maka yang dianggap benar adalah catatan pembukuan <b>PENGELOLA</b>.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">6.</td>
            <td colspan="2" class="kanankiri">
                Sepanjang mengenai kewajiban-kewajiban pembayaran <b>PEMBELI</b> kepada <b>PENGELOLA</b> yang timbul dari Akad ini, maka <b>PEMBELI</b> dengan ini memberi kuasa kepada <b>PENGELOLA</b> untuk meminta dan menerima bagian dari gaji dan atau penerimaan lainnya yang menjadi hak <b>PEMBELI</b> dari pejabat yang berwenang membayarkan gaji dan atau penerimaan lainnya dari Instansi/Kantor dimana <b>PEMBELI</b> bekerja untuk pembayaran angsuran/Hutang <b>PEMBELI</b> .
            </td>
        </tr>
        <tr>
            <td class="tengah" colspan="3">
                <b>
                    <br>PASAL 8<br>
                    DENDA TUNGGAKAN<br><br>
                </b>
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">1.</td>
            <td colspan="2" class="kanankiri">
                Kewajiban angsuran yang tidak dilunasi merupakan tunggakan angsuran.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">2.</td>
            <td colspan="2" class="kanankiri">
                Dalam hal terjadi kelambatan pembayaran oleh <b>PEMBELI</b> kepada <b>PENGELOLA</b>, maka <b>PEMBELI</b> berjanji dan dengan ini mengikatkan diri untuk membayar :
            </td>
        </tr>
        <tr>
            <td width="20px"></td>
            <td valign="top" width="20px">a.</td>
            <td colspan="2" class="kanankiri">
                Gantirugi kerugian <b>PENGELOLA</b> dalam rangka melakukan penagihan kepada <b>PEMBELI</b> , meliputi tetapi tidak terbatas pada biaya komunikasi, transportasi, dan/atau akomodasi penagihan.
            </td>
        </tr>
        <tr>
            <td width="20px"></td>
            <td valign="top" width="20px">b.</td>
            <td colspan="2" class="kanankiri">
                @php
                $denda = terbilang($data->pasal8_a);
                @endphp
                Denda keterlambatan pada <b>PENGELOLA</b> sebesar Rp. 0 (Nol rupiah) untuk tiap-tiap hari kelambatan.
            </td>
        </tr>
        <tr>
            <td class="tengah" colspan="3">
                <b>
                    <br>PASAL 9<br>
                    UANG MUKA<br><br>
                </b>
            </td>
        </tr>
        <tr>
            <td colspan="3" class="kanankiri">
                <b>PENGELOLA</b> dapat meminta kepada <b>PEMBELI</b> uang muka (urbun) untuk pembelian Barang pada Akad ini dengan ketentuan sebagai berikut:
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">1.</td>
            <td colspan="2" class="kanankiri">
                Uang muka tersebut menjadi bagian pelunasan Hutang <b>PEMBELI</b> apabila Pembiayaan Murabahah dilaksanakan.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">2.</td>
            <td colspan="2" class="kanankiri">
                Apabila <b>PEMBELI</b> membatalkan Akad ini maka uang muka dikembalikan kepada <b>PEMBELI</b> setelah dikurangi dengan kerugian atau biaya yang telah dikeluarkan oleh <b>PENGELOLA</b>, jika uang muka lebih kecil dari kerugian <b>PENGELOLA</b> maka <b>PENGELOLA</b> dapat meminta tambahan dari <b>PEMBELI</b> .
            </td>
        </tr>
        <tr>
            <td class="tengah" colspan="3">
                <b>
                    <br>PASAL 10<br>
                    PELUNASAN DIPERCEPAT<br><br>
                </b>
            </td>
        </tr>
        <tr>
            <td colspan="3" class="kanankiri">
                Menyimpang dari pembayaran angsuran, <b>PEMBELI</b> dapat melakukan Pelunasan Dipercepat seluruh sisa kewajiban yang belum dilunasi yang dilakukan sebelum berakhirnya jatuh tempo Pembiayaan.
            <br><br><br><br>
            </td>
        </tr>
    </tbody>
</table>