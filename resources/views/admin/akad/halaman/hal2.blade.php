<table width="100%">
    <tbody>
        <tr>
            <td>d.</td>
            <td>Kegunaan/Jenis Pembiayaan</td>
            <td>: Pembiayaan & pengelolaan bidang tanah/kaveling</td>
        </tr>
        <tr>
            <td>e.</td>
            <td>Jangka Waktu Pembiayaan</td>
            <td>: ....... bulan</td>
        </tr>
        <tr>
            <td>f.</td>
            <td>Jatuh Tempo Pembiayaan</td>
            <td>: Setiap Tanggal 10</td>
        </tr>
        <tr>
            <td>g.</td>
            <td>Angsuran per bulan</td>
            <td>: Rp. ...................(...............................) perbulan</td>
        </tr>
        <tr>
            <td>h.</td>
            <td>Jatuh Tempo Pembayaran Angsuran</td>
            <td>: Setiap tanggal 10 setiap bulan</td>
        </tr>
        <tr>
            <td>i.</td>
            <td>Jenis Jaminan</td>
            <td>: Surat Kepemilikan tanah/kaveling yang dibeli</td>
        </tr>
        <tr>
            <td>j.</td>
            <td>Bukti Kepemilikan Jaminan</td>
            <td>: Surat Sporadik/Sertifikat Hak Milik</td>
        </tr>
        <tr>
            <td>k.</td>
            <td>Nama Pemilik Aset</td>
            <td>: ...............................................................</td>
        </tr>
        <tr>
            <td class="tengah" colspan="3">
                <b>
                    <br>PASAL 2<br>
                    DEFINISI
                </b>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                Dalam Akad ini, yang dimaksud dengan :
            </td>
        </tr>
        <tr>
            <td valign="top">1.</td>
            <td class="kanankiri" colspan="2">
                <b>Akad</b> adalah perjanjian tertulis tentang fasilitas Pembiayaan dan Pengelolaan yang dibuat oleh <b>PENGELOLA</b> dan <b>PEMBELI</b> memuat ketentuan-ketentuan dan syarat- syarat yang disepakati, berikut perubahan-perubahan dan tambahan-tambahannya (addendum), sesuai dengan ketentuan dan perundang-undangan.
            </td>
        </tr>
        <tr>
            <td valign="top">2.</td>
            <td class="kanankiri" colspan="2">
                <b>PENGELOLA</b> adalah penyedia layanan pembiayaan dan pengelola berbasis teknologi dengan prinsip syariah yang menghimpunan dana dari pembeli dan yang menyediakan fasilitas pembiayaan kepada <b>PEMBELI</b> atas pembelian bidang tanah/kaveling.
            </td>
        </tr>
        <tr>
            <td valign="top">3.</td>
            <td class="kanankiri" colspan="2">
                <b>Bidang Tanah/kaveling</b> adalah berupa Tanah dengan ukuran tertentu yang kelola oleh KOPERASI MITRA GRIYA INDONESIA untuk kepentingan <b>PEMBELI</b>.
            </td>
        </tr>
        <tr>
            <td valign="top">4.</td>
            <td class="kanankiri" colspan="2">
                <b>PEMBELI</b> adalah Pembeli tanah/kaveling dengan ukuran tertentu dengan pembiayaan dikelola oleh KOPERASI MITRA GRIYA INDONESIA.
            </td>
        </tr>
        <tr>
            <td valign="top">5.</td>
            <td class="kanankiri" colspan="2">
                <b>Pembiayaan dan Pengelolaan</b> adalah penyediaan layanan pembiayaan dan pengelolaan bidang tanah/kaveling berdasarkan persetujuan atau kesepakatan antara <b>PENGELOLA</b> dengan <b>PEMBELI</b> untuk pembelian bidang Tanah/Kaveling dengan perjanjian cicilan dalam jangka waktu tertentu dengan marjin keuntungan.
            </td>
        </tr>
        <tr>
            <td valign="top">6.</td>
            <td class="kanankiri" colspan="2">
                <b>Harga Beli</b> adalah sejumlah uang yang harus dibayar oleh <b>PEMBELI</b> kepada PEMILIK yaitu bidang tanah/kaveling disetujui oleh <b>PEMBELI</b>
            </td>
        </tr>
        <tr>
            <td valign="top">7.</td>
            <td class="kanankiri" colspan="2">
                <b>Harga Jual</b> adalah harga beli ditambah marjin keuntungan PENGELOLA yang ditetapkan oleh <b>PENGELOLA</b> dan disetujui/disepakati oleh <b>PEMBELI</b>/<b>PEMBELI</b> dan merupakan jumlah Pembiayaan.
            </td>
        </tr>
        <tr>
            <td valign="top">8.</td>
            <td class="kanankiri" colspan="2">
                <b>Marjin Keuntungan</b> adalah jumlah uang yang wajib dibayar <b>PEMBELI</b> kepada <b>PENGELOLA</b> sebagai imbalan atas Pembiayaan yang diberikan oleh <b>PENGELOLA</b>, yang merupakan selisih antara Harga Jual dan Harga Beli.
            </td>
        </tr>
        <tr>
            <td valign="top">9.</td>
            <td class="kanankiri" colspan="2">
                <b>Uang Muka</b> adalah sejumlah uang yang besarnya ditetapkan oleh <b>PENGELOLA</b> dan disetujui oleh <b>PEMBELI</b> yang harus dibayarkan terlebih dahulu oleh <b>PEMBELI</b> kepada <b>PENGELOLA</b> sebagai salah satu syarat yang harus dipenuhi <b>PENGELOLA</b> untuk memperoleh Pembiayaan Murabahah dari <b>PENGELOLA</b>.
            </td>
        </tr>
        <tr>
            <td valign="top">10.</td>
            <td class="kanankiri" colspan="2">
                <b>Piutang</b> adalah hak tagih <b>PENGELOLA</b> kepada <b>PEMBELI</b> yang timbul karena <b>PEMBELI</b> telah menerima fasilitas pembiayaan dari <b>PEMBELI</b> dan besarnya adalah sama dengan Harga Jual.
            </td>
        </tr>
        <tr>
            <td valign="top">11.</td>
            <td class="kanankiri" colspan="2">
                <b>Angsuran</b> adalah sejumlah uang untuk pembayaran Jumlah Harga Jual yang wajib dibayar secara bulanan oleh <b>PEMBELI</b> kepada <b>PENGELOLA</b> sebagaimana ditentukan Akad ini.
            </td>
        </tr>
    </tbody>
</table>