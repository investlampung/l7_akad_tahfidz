<table width="100%">
    <tbody>
        <tr>
            <td class="tengah" colspan="3">
                <b>
                    <br>PASAL 19<br>
                    HUKUM YANG BERLAKU<br><br>
                </b>
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">1.</td>
            <td colspan="2" class="kanankiri">
                Pelaksanaan Akad ini tunduk kepada ketentuan perundang-undangan yang berlaku di Indonesia dan ketentuan Syariah yang berlaku bagi <b>PENGELOLA</b>
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">2.</td>
            <td colspan="2" class="kanankiri">
                Apabila dikemudian hari terjadi perselisihan dalam penafsiran atau pelaksanaan ketentuan- ketentuan dari Akad ini, maka para pihak sepakat untuk terlebih dahulu menyelesaikan secara musyawarah.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">3.</td>
            <td colspan="2" class="kanankiri">
                Bilamana musyawarah tidak menghasilkan kata sepakat mengenai penyelesaian perselisihan, maka semua sengketa yang timbul dari Akad ini akan diselesaikan dan diputus oleh Pengadialan Agama yang keputusannya mengikat kedua belah pihak yang bersengketa, sebagai keputusan tingkat pertama dan terakhir.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">4.</td>
            <td colspan="2" class="kanankiri">
                Mengenai pelaksanaan (eksekusi) putusan Pengadilan Agama, sesuai dengan ketentuan Undang- Undang tentang Arbitrase dan Alternatif Penyelesaian Sengketa, PARA PIHAK sepakat bahwa <b>PENGELOLA</b> dapat meminta pelaksanaan (eksekusi) putusan Pengadilan Agama tersebut pada setiap Pengadilan Negeri di wilayah hukum Republik Indonesia.
            </td>
        </tr>
        <tr>
            <td class="tengah" colspan="3">
                <b>
                    <br>PASAL 20<br>
                    LAIN - LAIN<br><br>
                </b>
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">1.</td>
            <td colspan="2" class="kanankiri">
                Dalam hal <b>PEMBELI</b> menyampaikan pernyataan yang tidak benar mengenai financing to value ratio maka <b>PEMBELI</b> bersedia melaksanakan langkah-langkah yang ditetapkan oleh <b>PENGELOLA</b> dalam rangka pemenuhan ketentuan <b>PENGELOLA</b> Indonesia atau institusi yang berwenang.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">2.</td>
            <td colspan="2" class="kanankiri">
                Semua pemberitahuan tertulis dari <b>PENGELOLA</b> dan semua surat menyurat antara <b>PENGELOLA</b> dan <b>PEMBELI</b> dalam pelaksanaan Akad ini mengikat dan harus ditaati oleh <b>PEMBELI</b> .
            <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
            </td>
        </tr>
    </tbody>
</table>