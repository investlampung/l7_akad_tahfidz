<table width="100%">
    <tbody>
        <tr>
            <td class="tengah" colspan="3">
                <b>
                    <br>PASAL 11<br>
                    JAMINAN DAN PENGIKATNYA<br><br>
                </b>
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">1.</td>
            <td colspan="2" class="kanankiri">
                Guna menjamin pembayaran kembali Hutang Murabahah, <b>PEMBELI</b> wajib menyerahkan Barang yang dibiayai sebagai jaminan, serta menyerahkan bukti-bukti kepemilikan jaminan yang asli dan sah untuk diikat sesuai dengan ketentuan peraturan perundang-undangan yang berlaku.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">2.</td>
            <td colspan="2" class="kanankiri">
                <b>PEMBELI</b> wajib memberikan bantuan sepenuhnya guna memungkinkan <b>PENGELOLA</b> melaksanakan pengikatan Barang yang dibiayai dengan fasilitas Pembiayaan sebagai jaminan menurut cara dan pada saat yang dianggap baik oleh <b>PENGELOLA</b>. Bukti Kepemilikan Barang dan Pengikatan Barang Jaminan dikuasai oleh <b>PENGELOLA</b> sampai seluruh jumlah Pembiayaan dilunasi.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">3.</td>
            <td colspan="2" class="kanankiri">
                Seluruh biaya dalam pengikatan Barang Jaminan menjadi tanggungan <b>PENJUAL</b>.
            </td>
        </tr>
        <tr>
            <td class="tengah" colspan="3">
                <b>
                    <br>PASAL 12<br>
                    PEMBELI WANPRESTASI<br><br>
                </b>
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">1.</td>
            <td colspan="2" class="kanankiri">
                <b>PEMBELI</b> dinyatakan wanprestasi, apabila tidak memenuhi dengan baik kewajiban-kewajibannya atau melanggar ketentuan-ketentuan di dalam Akad ini.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">2.</td>
            <td colspan="2" class="kanankiri">
                Apabila <b>PEMBELI</b> wanprestasi, <b>PENGELOLA</b> berhak untuk memberikan peringatan dalam bentuk tindakan-tindakan sebagai berikut:
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px">a.</td>
            <td colspan="2" class="kanankiri">
                Memberikan peringatan baik secara lisan maupun dalam bentuk pernyataan lalai/wanprestasi berupa surat atau akta lain yang sejenis yang dikirimkan ke alamat <b>PEMBELI</b>.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px">b.</td>
            <td colspan="2" class="kanankiri">
                Mengadakan dan menjadwalkan musyawarah tentang solusi bersama secara adil.
            </td>
        </tr>

        <tr>
            <td class="tengah" colspan="3">
                <b>
                    <br>PASAL 13<br>
                    PENGAWASAN, PEMERIKSAAN DAN TINDAKAN TERHADAP BARANG JAMINAN<br><br>
                </b>
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">1.</td>
            <td colspan="2" class="kanankiri">
                Selama <b>PEMBELI</b> belum melunasi seluruh Hutang Murabahah yang timbul dari Akad ini, <b>PENGELOLA</b> belum menyerahkan surat legalitas tanah/kaveling baik itu surat sporadik maupun SHM.
            </td>
        </tr>
        <tr>
            <td class="tengah" colspan="4">
                <b>
                    <br>PASAL 14<br>
                    TANGGUNG JAWAB PARA-PIHAK<br><br>
                </b>
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">1.</td>
            <td colspan="3" class="kanankiri">
                Pilihan atas Barang yang akan dibeli dengan Pembiayaan <b>PENGELOLA</b>, sepenuhnya menjadi tanggung jawab <b>PEMBELI</b> sebagai pembeli.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">2.</td>
            <td colspan="3" class="kanankiri">
                Apabila kemudian hari diketahui atau timbul cacat, kekurangan atau keadaan/masalah apapun yang menyangkut Barang dan atau pelaksanaan Akad/Akta Jual Beli barang dan tanah, jual beli mana seluruh atau sebagian dibiayai dengan Pembiayaan <b>PENGELOLA</b>, maka segala risiko sepenunya menjadi tanggung jawab <b>PENGELOLA</b> .
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">3.</td>
            <td colspan="3" class="kanankiri">
                Adanya cacat kekurangan atau masalah yang timbul tidak dapat dijadikan alasan untuk mengingkari, melalaikan atau menunda pelaksanaan kewajiban <b>PEMBELI</b> kepada <b>PENGELOLA</b> sesuai Akad ini, termasuk antara lain membayar angsuran dan sebagainya.
            </td>
        </tr>
    </tbody>
</table>