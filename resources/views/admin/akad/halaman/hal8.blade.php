<table width="100%">
    <tbody>
        <tr>
            <td class="tengah" colspan="3">
                <b>
                    <br>PASAL 16<br>
                    PENGUASAAN DAN PENJUALAN (EKSEKUSI) BARANG JAMINAN<br><br>
                </b>
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">1.</td>
            <td colspan="2" class="kanankiri">
                Apabila <b>PEMBELI</b> wanprestasi, maka setelah memperingatkan <b>PEMBELI</b> , <b>PENGELOLA</b> berhak untuk melakukan tindakan-tindakan sebagai berikut:
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px">a.</td>
            <td colspan="2" class="kanankiri">
                Melaksanakan musyawarah mufakat terhadap barang jaminan berdasarkan ketentuan yang telah disepakati bersama.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px">b.</td>
            <td colspan="2" class="kanankiri">
                Melaksanakan penjualan terhadap barang jaminan berdasarkan Surat Kuasa Untuk Menjual yang dibuat oleh <b>PEMBELI</b> .Dengan harga dan pembagian sesuai kesepakatan bersama.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px">c.</td>
            <td colspan="2" class="kanankiri">
                Menetapkan harga penjualan dengan harga yang dianggap baik oleh <b>PENGELOLA</b>.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">2.</td>
            <td colspan="2" class="kanankiri">
                Hasil penjualan barang jaminan tersebut diprioritaskan untuk melunasi seluruh sisa Hutang <b>PEMBELI</b> kepada <b>PENGELOLA</b>, termasuk semua biaya yang telah dikeluarkan <b>PENGELOLA</b> guna melaksanakan penjualan Barang jaminan, dan apabila masih ada sisanya maka jumlah sisa tersebut akan dibayarkan kepada <b>PEMBELI</b>.dengan bagi hasil tertentu.
            </td>
        </tr>
        <tr>
            <td class="tengah" colspan="3">
                <b>
                    <br>PASAL 17<br>
                    KUASA YANG TIDAK DAPAT DITARIK KEMBALI<br><br>
                </b>
            </td>
        </tr>
        <tr>
            <td colspan="3" class="kanankiri">
                Semua kuasa yang dibuat dan diberikan oleh <b>PEMBELI</b> dalam rangka Akad ini merupakan satu kesatuan yang tak terpisahkan dari Akad ini dan tidak dapat ditarik kembali karena sebab-sebab apapun juga yang dapat mengakhiri kuasa terutama yang dimaksud dalam Pasal 1813 Kitab Undang-Undang Hukum Perdata sampai dengan Pembiayaan lunas, dan <b>PEMBELI</b> mengikatkan serta mewajibkan diri untuk tidak membuat surat-surat kuasa dan atau janji-janji yang sifat dan atau isinya serupa kepada pihak lain, selain kepada <b>PENGELOLA</b>.
            </td>
        </tr>
        <tr>
            <td class="tengah" colspan="3">
                <b>
                    <br>PASAL 18<br>
                    ALAMAT PIHAK-PIHAK<br><br>
                </b>
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">1.</td>
            <td colspan="2" class="kanankiri">
                Seluruh pembayaran Hutang atau setiap bagian dari Hutang <b>PEMBELI</b> dan surat menyurat harus dilakukan/dialamatkan pada Kantor <b>PENGELOLA</b> yang telah ditentukan pada jam-jam kerja dari Kantor yang bersangkutan.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">2.</td>
            <td colspan="2" class="kanankiri">
                Semua surat menyurat dan pernyataan tertulis yang timbul dari dan bersumber pada Akad ini dianggap telah diserahkan dan diterima apabila dikirimkan kepada :
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px">a.</td>
            <td colspan="2" class="kanankiri">
                Pihak <b>PENGELOLA</b> dengan alamat Kantor <b>PENGELOLA</b> yang bersangkutan.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px">b.</td>
            <td colspan="2" class="kanankiri">
                <b>PEMBELI</b> dengan alamat barang atau alamat Kantor <b>PEMBELI</b> yang tercantum pada formulir permohonan Pembiayaan atau alamat yang tercantum pada Akad ini.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">3.</td>
            <td colspan="2" class="kanankiri">
                Kedua belah pihak masing-masing akan memberitahukan secara tertulis pada kesempatan pertama/secepatnya setiap terjadi perubahan alamat, <b>PEMBELI</b> pindah/tidak lagi menghuni barang yang bersangkutan dan sebagainya.
            <br><br>
            </td>
        </tr>
    </tbody>
</table>