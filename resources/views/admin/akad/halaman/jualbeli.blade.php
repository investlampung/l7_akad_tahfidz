<table width="100%">
    <tbody>
        <tr>
            <td width="180px"><img src="{{asset('itlabil/images/default/logo_beranda.png')}}" alt="" width="150px"></td>
            <td valign="middle"><b>
                    <h2>SURAT JUAL BELI TANAH</h2>
                </b></td>
        </tr>
    </tbody>
</table>
<table width="100%">
    <tbody>
        <tr>
            <td colspan="4">Yang bertanda tangan dibawah ini saya :</td>
        </tr>
        <tr>
            <td width="5%"></td>
            <td>1.</td>
            <td width="20%">Nama</td>
            <td>: Wahyudi</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>Umur/TTL</td>
            <td>: 39 Tahun</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>Pekerjaan</td>
            <td>: Wiraswasta</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td valign="top">Alamat</td>
            <td>
                : Gadingrejo, RT/RW : 01/07 Pekon Gadingrejo Kec. Gadingrejo Kab. Pringsewu
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td colspan="2">
                Dalam hal ini disebut Pihak Kesatu <b>(Penjual)</b>
            </td>
        </tr>
        <tr height="20px">
            <td colspan="4"></td>
        </tr>
        <tr>
            <td width="5%"></td>
            <td>2.</td>
            <td>Nama</td>
            <td>: {{$data->anggota->nama}}</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>Umur/TTL</td>

            @if(empty($data->anggota->tanggal_lahir))
            <td>: {{$data->anggota->tempat_lahir}},</td>
            @else
            @php
            $mmm = $data->anggota->tanggal_lahir;
            $age = Carbon::parse($mmm)->age;
            @endphp
            <td>: {{$age}} Tahun - {{$data->anggota->tempat_lahir}}, {{$data->anggota->tanggal_lahir->format('d-m-Y')}}</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td valign="top">Alamat</td>
            <td>
                : {{$data->anggota->alamat}}
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td colspan="2">
                Dalam hal ini disebut Pihak Kedua <b>(Pembeli)</b>
            </td>
        </tr>
        <tr height="20px">
            <td colspan="4"></td>
        </tr>
        <tr>
            <td colspan="4">
                Pada Hari ini, {{tanggal_indonesia($data->tanggal)}} menyatakan :
            </td>
        </tr>
        <tr>
            <td></td>
            <td valign="top">1.</td>
            <td colspan="2" class="kanankiri">
                Pihak kesatu menyatakan dengan sesungguhnya telah menjual bidang berupa tanah kapling dengan ukuran 10 m x 15 m (150 M2) yang terletak di blok ……………… RW 06/07 Pekon Gadingrejo kecamatan Gadingrejo kabupaten Pringsewu.
            </td>
        </tr>
        <tr>
            <td></td>
            <td valign="top">2.</td>
            <td colspan="2" class="kanankiri">
                Harga jual bidang kavling di atas adalah sebesar 45 juta rupiah dengan cara pembayaran secara kredit.
            </td>
        </tr>
        <tr>
            <td></td>
            <td valign="top">3.</td>
            <td colspan="2" class="kanankiri">
                Mulai hari ini dan seterusnya tanah tersebut sepenuhnya menjadi hak milik Pihak Kedua.
            </td>
        </tr>
        <tr>
            <td></td>
            <td valign="top">4.</td>
            <td colspan="2" class="kanankiri">
                Surat Jual beli ini dipergunakan untuk mengurus surat keterangan jual beli dan surat kepemilikan sporadik yang diketahui oleh aparat Pemerintahan Pekon gadingrejo.
            </td>
        </tr>
        <tr height="20px">
            <td colspan="4"></td>
        </tr>
        <tr>
            <td colspan="4" class="kanankiri">
                Demikian Surat keterangan ini dibuat dengan sebenarnya dalam keadaan sehat jasmani dan rohani dengan itikad baik untuk dipergunakan sebagaimana mestinya.
            </td>
        </tr>
    </tbody>
</table>
<table width="100%" style="margin-top: 20px;">
    <tbody>
        <tr>
            <td align="right">Gadingrejo,</td>
            <td width="25%">
                {{tanggal_local($data->tanggal)}}
            </td>
        </tr>
    </tbody>
</table>
<table width="100%">
    <tbody>
        <tr>
            <td align="center" width="50%">
                Pihak Kedua<br>
                Pembeli<br><br><br><br><br><br><br><br>
                <b>( {{$data->anggota->nama}} )</b>
            </td>
            <td align="center">
                Pihak Kesatu<br>
                Penjual<br>
                <img src="{{asset('itlabil/images/default/ttd.png')}}" alt="" width="150px">
                <br>
                <b>( WAHYUDI )</b>
            </td>
        </tr>
    </tbody>
</table>