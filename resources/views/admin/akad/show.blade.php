<!DOCTYPE html>
<html>

<head>
    <title>AKAD - {{$data->anggota->nama}}</title>
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro&display=swap" rel="stylesheet">
    <link href="{{ asset('itlabil/admin/bower_components/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <!-- <link href="{{ asset('itlabil/admin/bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet"> -->

    <style>
        @media print {
            @page {
                size: auto !important
            }
        }

        #formulir {
            background-color: #000000;
            color: #ffffff;
        }

        .samping {
            padding: 15px 15px;
        }

        body {
            font-family: 'Source Sans Pro', sans-serif;
            font-size: 15px;
            margin: 15px 15px;
        }

        .kanankiri {
            text-align: justify;
        }

        .tengah {
            text-align: center;
        }

        .table-a {
            border-collapse: collapse;
        }

        .table-a,
        .th,
        .tr,
        .td {
            border: 1px solid black;
            padding-left: 5px;
            padding-right: 5px;
        }
    </style>
</head>

<body>
    <div class="container">

        <!-- FORMULIR -->
        @include('admin.akad.halaman.formulir')
        @include('admin.akad.halbar')

        <!-- SURAT JUAL BELI -->
        @include('admin.akad.halaman.jualbeli')
        @include('admin.akad.halbar')

        @php
        $no=1;
        @endphp

        <!-- HALAMAN 1 -->
        @include('admin.akad.header')
        @include('admin.akad.halaman.hal1')
        @include('admin.akad.footer')
        @include('admin.akad.halbar')

        @php
        $no++;
        @endphp

        <!-- HALAMAN 2 -->
        @include('admin.akad.header')
        @include('admin.akad.halaman.hal2')
        @include('admin.akad.footer')
        @include('admin.akad.halbar')

        @php
        $no++;
        @endphp

        <!-- HALAMAN 3 -->
        @include('admin.akad.header')
        @include('admin.akad.halaman.hal3')
        @include('admin.akad.footer')
        @include('admin.akad.halbar')

        @php
        $no++;
        @endphp

        <!-- HALAMAN 4 -->
        @include('admin.akad.header')
        @include('admin.akad.halaman.hal4')
        @include('admin.akad.footer')
        @include('admin.akad.halbar')

        @php
        $no++;
        @endphp

        <!-- HALAMAN 5 -->
        @include('admin.akad.header')
        @include('admin.akad.halaman.hal5')
        @include('admin.akad.footer')
        @include('admin.akad.halbar')

        @php
        $no++;
        @endphp

        <!-- HALAMAN 6 -->
        @include('admin.akad.header')
        @include('admin.akad.halaman.hal6')
        @include('admin.akad.footer')
        @include('admin.akad.halbar')

        @php
        $no++;
        @endphp

        <!-- HALAMAN 7 -->
        @include('admin.akad.header')
        @include('admin.akad.halaman.hal7')
        @include('admin.akad.footer')
        @include('admin.akad.halbar')

        @php
        $no++;
        @endphp

        <!-- HALAMAN 8 -->
        @include('admin.akad.header')
        @include('admin.akad.halaman.hal8')
        @include('admin.akad.footer')
        @include('admin.akad.halbar')

        @php
        $no++;
        @endphp

        <!-- HALAMAN 9 -->
        @include('admin.akad.header')
        @include('admin.akad.halaman.hal9')
        @include('admin.akad.footer')
        @include('admin.akad.halbar')

        @php
        $no++;
        @endphp

        <!-- HALAMAN 10 -->
        @include('admin.akad.header')
        @include('admin.akad.halaman.hal10')
        @include('admin.akad.footer')
        @include('admin.akad.halbar')

        @php
        $no++;
        @endphp

        <!-- HALAMAN 11 -->
        @include('admin.akad.header')
        @include('admin.akad.halaman.hal11')
        @include('admin.akad.footer')

    </div>

</body>

</html>