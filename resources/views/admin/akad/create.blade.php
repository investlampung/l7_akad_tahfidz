@extends('layouts.admin')

@section('content')
<div class="container">

    <section class="content-header">
        <h1>
            Akad
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Tambah Data Akad</h3>
                </div>

                <div class="box-body" style="overflow-x:auto;">
                    <form action="{{ route('admin.akad.store') }}" class="form-horizontal" method="POST">
                        @csrf

                        <div class="col-md-12">
                            <hr>
                            Anggota
                            <hr>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Nama</label>
                                <div class="col-sm-8">
                                    <select class="select2 form-control" name="anggota_id">
                                        <option value="-">-</option>
                                        @foreach($anggota as $ang)
                                        <option value="{{$ang->id}}">{{$ang->nama}}</option>
                                        @endforeach
                                    </select>
                                    <small class="text-danger">{{ $errors->first('anggota_id') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Nomor</label>

                                <div class="col-sm-8">
                                    <input type="text" name="nomor" class="form-control" placeholder="Nomor" >
                                    <small class="text-danger">{{ $errors->first('nomor') }}</small>
                                </div>
                            </div>
                            <hr>
                            Pasal 5
                            <hr>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Jangka Waktu</label>

                                <div class="col-sm-8">
                                    <input type="number" name="pasal5_a" class="form-control" placeholder="Jangka Waktu / Bulan">
                                    <small class="text-danger">{{ $errors->first('pasal5_a') }}</small>
                                </div>
                            </div>
                            <hr>
                            Tanggal
                            <hr>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Tanggal</label>

                                <div class="col-sm-8">
                                    <input type="date" name="tanggal" class="form-control">
                                    <small class="text-danger">{{ $errors->first('tanggal') }}</small>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                            <a href="{{url('admin/akad')}}" class="btn btn-default  pull-right">Kembali</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('.select2').select2({
        ajax: {
            url: '{{asset('admin-akad-ajax')}}',
            dataType: 'json',
            delay: 250,
            processResults: function(data) {
                return {
                    results: $.map(data, function(item) {
                        return {
                            text: item.nama,
                            id: item.id
                        }
                    })
                };
            },
            cache: true
        }
    });
</script>
@endsection