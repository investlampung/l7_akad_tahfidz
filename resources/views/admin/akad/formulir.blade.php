<!DOCTYPE html>
<html>

<head>
    <title>Formulir</title>
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro&display=swap" rel="stylesheet">
    <!-- <link href="{{ asset('itlabil/admin/bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet"> -->

    <style>
        @media print {
            @page {
                size: auto !important
            }
        }

        #formulir {
            background-color: #000000;
            color: #ffffff;
        }

        .samping {
            padding: 15px 15px;
        }

        body {
            font-family: 'Source Sans Pro', sans-serif;
            margin: 15px 15px;
        }

        .kanankiri {
            text-align: justify;
        }

        .tengah {
            text-align: center;
        }

        .table-a {
            border-collapse: collapse;
        }

        .table-a,
        .tr,
        .td {
            border: 1px solid black;
            padding-left: 5px;
            padding-right: 5px;
        }
    </style>
</head>

<body onload="window.print()">
    <div class="container">
        <table width="100%">
            <tbody>
                <tr id="formulir">
                    <td class="tengah"><b>FORMULIR PENDAFTARAN</b></td>
                </tr>
                <tr>
                    <td class="tengah"><b>KAMPUNG TAHFIDZ GADINGREJO</b></td>
                </tr>
                <tr>
                    <td>Bismillahirohmanirrohim</td>
                </tr>
                <tr>
                    <td>saya bertanda tangan dibwah ini :</td>
                </tr>
            </tbody>
        </table>
        <table width="100%" class="table-a" style="background-color: #dedede;">
            <tr class="tr">
                <td class="td" width="35%">NOMOR</td>
                <td class="td tengah" width="5%">:</td>
                <td class="td" colspan="5"></td>
            </tr>
            <tr class="tr">
                <td class="td">NAMA</td>
                <td class="td tengah">:</td>
                <td class="tdh" colspan="5"></td>
            </tr>
            <tr class="tr">
                <td class="td">NIK</td>
                <td class="td tengah">:</td>
                <td class="td" colspan="5"></td>
            </tr>
            <tr class="tr">
                <td class="td">TEMPAT /TGL LAHIR</td>
                <td class="td tengah">:</td>
                <td class="td" colspan="5"></td>
            </tr>
            <tr class="tr">
                <td class="td"></td>
                <td class="td tengah">:</td>
                <td class="td" width="10%" style="background-color: #ffffff;"></td>
                <td class="td" colspan="2">Perempuan</td>
                <td class="td" width="10%" style="background-color: #ffffff;"></td>
                <td class="td">Laki - Laki</td>
            </tr>
            <tr class="tr">
                <td class="td">ALAMAT</td>
                <td class="td tengah">:</td>
                <td class="td" style='border:none;'>Desa</td>
                <td class="td" colspan="4" style='border:none;'>:</td>
            </tr>
            <tr class="tr">
                <td class="td"></td>
                <td class="td tengah"></td>
                <td class="td" style='border:none;'>Kec.</td>
                <td class="td" colspan="3" style='border:none;'>:</td>
                <td class="td" style='border:none;'>Kode Pos :</td>
            </tr>
            <tr class="tr">
                <td class="td"></td>
                <td class="td tengah"></td>
                <td class="td" style='border:none;'>Kab.</td>
                <td class="td" colspan="4" style='border:none;'>:</td>
            </tr>
            <tr class="tr">
                <td class="td">JENIS IDENTITAS</td>
                <td class="td tengah">:</td>
                <td class="td" colspan="5">KTP</td>
            </tr>
            <tr class="tr">
                <td class="td">NO WA</td>
                <td class="td tengah">:</td>
                <td class="td" colspan="3" style='border:none;'>Istri:</td>
                <td class="td" colspan="2" style='border:none;'>Suami:</td>
            </tr>
            <tr class="tr">
                <td class="td">1/3 Kaveling ditanam pohon jambu</td>
                <td class="td tengah">:</td>
                <td class="td" style="background-color: #ffffff;"></td>
                <td class="td" colspan="2">Setuju</td>
                <td class="td" style="background-color: #ffffff;"></td>
                <td class="td">Tidak Setuju</td>
            </tr>
            <tr class="tr">
                <td class="td">Kepemilikan diteruskan menjadi SHM db biaya mandiri</td>
                <td class="td tengah">:</td>
                <td class="td" style="background-color: #ffffff;"></td>
                <td class="td" colspan="2">Setuju</td>
                <td class="td" style="background-color: #ffffff;"></td>
                <td class="td">Tidak Setuju</td>
            </tr>
            <tr class="tr">
                <td class="td">REKOMENDASI/INFO DARI</td>
                <td class="td tengah">:</td>
                <td class="td" colspan="5"></td>
            </tr>
            <tr class="tr">
                <td class="td" rowspan="3">PILIHAN PEMBAYARAN</td>
                <td class="td tengah" rowspan="3">:</td>
                <td class="td" style="background-color: #ffffff;"></td>
                <td class="td" width="10%" colspan="4">CASH</td>
            </tr>
            <tr class="tr">
                <td class="td" style="background-color: #ffffff;"></td>
                <td class="td" style='border:none;'>DP</td>
                <td class="td" colspan="3" style='border:none;'>Rp :</td>
            </tr>
            <tr class="tr">
                <td class="td" style="background-color: #ffffff;"></td>
                <td class="td" style='border:none;'>KREDIT</td>
                <td class="td" colspan="" style='border:none;'>Rp :</td>
                <td class="td" style='border:none;'></td>
                <td class="td" align="right" style='border:none;'>per-bulan</td>
            </tr>
            <tr class="tr">
                <td class="td">BLOK KAVELING</td>
                <td class="td tengah">:</td>
                <td class="td" style="background-color: #ffffff;"></td>
                <td class="td" colspan="4"></td>
            </tr>
        </table>
        <table width="100%">
            <tbody>
                <tr>
                    <td colspan="4">Ketentuan :</td>
                </tr>
                <tr>
                    <td width="7%"></td>
                    <td width="3%" valign="top">1.</td>
                    <td colspan="2"><i>Ukuran bidang kaveling adalah 10 m x 15m atau 150M2</i></td>
                </tr>
                <tr>
                    <td></td>
                    <td valign="top">2.</td>
                    <td colspan="2"><i>Kelebihan ukuran tanah dihitung 300rb/M2</i></td>
                </tr>
                <tr>
                    <td></td>
                    <td valign="top">3.</td>
                    <td colspan="2"><i>Legalitas surat tanah adalah Surat jual beli dan Sporadik dari Pekon akan diberikan ke Pembeli setelah pelunasan Cicilan</i></td>
                </tr>
                <tr>
                    <td></td>
                    <td valign="top">4.</td>
                    <td colspan="2"><i>Pembelian dengan cara kredit maksimal lunas adalah 3 tahun</i></td>
                </tr>
                <tr>
                    <td></td>
                    <td valign="top">5.</td>
                    <td colspan="2"><i>Apabila 3 x berturut-turut konsumen tidak melakukan cicilan, maka Pihak Pengembang bermusyawarah dengan Pembeli untuk dicarikan jalan keluar</i></td>
                </tr>
                <tr>
                    <td></td>
                    <td valign="top">6.</td>
                    <td colspan="2"><i>Pembeli boleh membangun rumah diatas kaveling selama proses cicilan</i></td>
                </tr>
                <tr>
                    <td></td>
                    <td valign="top">7.</td>
                    <td colspan="2"><i>Pembeli tidak dibenarkan menstranfer/melakukan cicilan Selain rekening yayasan Ats tsaqib/ Petugas resmi bagian keuangan</i></td>
                </tr>
                <tr>
                    <td></td>
                    <td valign="top">8.</td>
                    <td width="23%"><i>Rekening Resmi A.N :</i></td>
                    <td><i>Yayasan Ats-Tsaqib </i></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><i>Bank Syariah Mandiri (451) </i></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><i>Nomor rekening : 7127781658 </i></td>
                </tr>
                <tr>
                    <td></td>
                    <td valign="top">9.</td>
                    <td colspan="2"><i>Pembeli berhak mendapatkan diskon jika melunasi lebih cepat dari waktu akad pertama</i></td>
                </tr>
            </tbody>
        </table>
        <table width="100%">
            <tbody>
                <tr>
                    <td align="right">Gadingrejo,</td>
                    <td width="20%"></td>
                    <td width="10%">
                        2020
                    </td>
                </tr>
            </tbody>
        </table>
        <table width="100%" class="table-a" style="background-color: #dedede;margin-top:10px">
            <tbody>
                <tr class="tr">
                    <td class="td" align="center">
                        Penjual<br><br><br><br><br>
                        ( WAHYUDI )
                    </td>
                    <td class="td" align="center">
                        Pembeli<br><br><br><br><br>
                        (......................)
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

</body>

</html>