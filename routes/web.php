<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return redirect('/login');
});
Route::get('/home', function () {
    return redirect('/admin/beranda');
});

Auth::routes([
    'register' => false, // Registration Routes...
    'reset' => false, // Password Reset Routes...
    'verify' => false, // Email Verification Routes...
]);

//ADMIN
//Beranda
Route::resource('admin/beranda', 'Web\Admin\BerandaController', ['as' => 'admin']);
Route::resource('admin/pekerjaan', 'Web\Admin\PekerjaanController', ['as' => 'admin']);
Route::resource('admin/anggota', 'Web\Admin\AnggotaController', ['as' => 'admin']);
Route::get('admin-anggota-ajax', 'Web\Admin\AnggotaController@dataAjax');
Route::resource('admin/akad', 'Web\Admin\AkadController', ['as' => 'admin']);
Route::get('admin-akad-ajax', 'Web\Admin\AkadController@dataAjax');
Route::resource('admin/angsuran', 'Web\Admin\AngsuranController', ['as' => 'admin']);
Route::get('admin/angsuran/index/{id}', 'Web\Admin\AngsuranController@index', ['as' => 'admin']);

Route::resource('admin/history', 'Web\Admin\HistoryController', ['as' => 'admin']);

Route::get('admin/upload/anggota', 'Web\Admin\UploadController@anggota')->name('admin.upload.anggota');
Route::post('admin/import/anggota', 'Web\Admin\UploadController@import_anggota')->name('admin.import.anggota');

Route::get('admin/upload/akad', 'Web\Admin\UploadController@akad')->name('admin.upload.akad');
Route::post('admin/import/akad', 'Web\Admin\UploadController@import_akad')->name('admin.import.akad');

Route::get('admin/upload/jpa', 'Web\Admin\UploadController@jpa')->name('admin.upload.jpa');
Route::post('admin/import/jpa', 'Web\Admin\UploadController@import_jpa')->name('admin.import.jpa');

Route::get('export', 'Web\Admin\DownloadController@export_anggota')->name('export');
//cteak formulir
Route::get('admin/akad/cetak/formulir', 'Web\Admin\AkadController@formulir')->name('admin.akad.cetak.formulir');
