<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Angsuran extends Model
{
    protected $guarded = [];

    protected $dates = ['tanggal'];
    public function akad()
    {
        return $this->belongsTo('App\Akad', 'akad_id');
    }
}
