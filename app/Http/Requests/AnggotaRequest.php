<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AnggotaRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        switch ($this->method()) {
            case 'POST': {
                    return [
                        'no_kapling'                => 'required'
                    ];
                }

            case 'PUT':
            case 'PATCH': {
                    return [
                        'no_kapling'                => 'required'
                    ];
                }

            default:
                break;
        }
    }
    public function messages()
    {
        return [

            'no_kapling.required' => 'Tidak boleh kosong'
        ];
    }
}
