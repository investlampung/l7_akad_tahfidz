<?php

namespace App\Http\Controllers\Web\Admin;

use App\History;
use App\Http\Controllers\Controller;
use App\Imports\ImportAkad;
use App\Imports\ImportAnggota;
use App\Imports\ImportJpa;
use Illuminate\Http\Request;

use Excel;
use Illuminate\Support\Facades\Auth;

class UploadController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function anggota()
    {
        return view('admin.upload.anggota');
    }

    public function import_anggota(Request $request)
    {
        if ($request->hasFile('file')) {
            $file = $request->file('file'); //GET FILE
            Excel::import(new ImportAnggota, $file); //IMPORT FILE 

            // HISTORI
            $histori['user'] = Auth::user()->name;
            $histori['info'] = "Tambah";
            $histori['desc_info'] = "Upload Data Anggota.";
            History::create($histori);

            $notification = array(
                'message' => 'Upload Data Anggota berhasil.',
                'alert-type' => 'info'
            );
            return redirect()->route('admin.anggota.index')->with($notification);
        }
    }

    public function akad()
    {
        return view('admin.upload.akad');
    }

    public function import_akad(Request $request)
    {
        if ($request->hasFile('file')) {
            $file = $request->file('file'); //GET FILE
            Excel::import(new ImportAkad, $file); //IMPORT FILE 

            // HISTORI
            $histori['user'] = Auth::user()->name;
            $histori['info'] = "Tambah";
            $histori['desc_info'] = "Upload Data Akad.";
            History::create($histori);

            $notification = array(
                'message' => 'Upload Data Akad berhasil.',
                'alert-type' => 'info'
            );
            return redirect()->route('admin.akad.index')->with($notification);
        }
    }

    public function jpa()
    {
        return view('admin.upload.jpa');
    }

    public function import_jpa(Request $request)
    {
        if ($request->hasFile('file')) {
            $file = $request->file('file'); //GET FILE
            Excel::import(new ImportJpa, $file); //IMPORT FILE 

            // HISTORI
            $histori['user'] = Auth::user()->name;
            $histori['info'] = "Tambah";
            $histori['desc_info'] = "Upload Data Jadwal Pembayaran Angsuran.";
            History::create($histori);

            $notification = array(
                'message' => 'Upload Data Jadwal Pembayaran Angsuran.',
                'alert-type' => 'info'
            );
            return redirect()->route('admin.akad.index')->with($notification);
        }
    }


    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
