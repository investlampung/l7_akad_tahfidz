<?php

namespace App\Http\Controllers\Web\Admin;

use App\Anggota;
use App\History;
use App\Http\Controllers\Controller;
use App\Http\Requests\AnggotaRequest;
use App\Pekerjaan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AnggotaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $no = 1;
        $data = Anggota::orderBy('nama', 'ASC')->get()->all();
        return view('admin.anggota.beranda', compact('data', 'no'));
    }
    public function dataAjax(Request $request)
    {
        $data = [];
        if ($request->has('q')) {
            $search = $request->q;
            $data = DB::table("pekerjaans")
                ->select("id", "pekerjaan")
                ->where('pekerjaan', 'LIKE', "%$search%")
                ->get();
        }


        return response()->json($data);
    }
    public function create()
    {
        $pekerjaan = Pekerjaan::orderBy('id', 'ASC')->get()->all();
        return view('admin.anggota.create', compact('pekerjaan'));
    }

    public function store(Request $request)
    {
        $data = $request->all();
        Anggota::create($data);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Tambah";
        $histori['desc_info'] = "Data Anggota " . $request->nama;
        History::create($histori);

        $notification = array(
            'message' => 'Data Anggota "' . $request->nama . '" berhasil ditambah.',
            'alert-type' => 'info'
        );

        return redirect()->route('admin.anggota.index')->with($notification);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $pekerjaan = Pekerjaan::orderBy('id', 'ASC')->get()->all();
        $data = Anggota::findOrFail($id);
        return view('admin.anggota.edit', compact('data', 'pekerjaan'));
    }

    public function update(Request $request, $id)
    {
        $data = Anggota::findOrFail($id);
        $data->update($request->all());

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Ubah";
        $histori['desc_info'] = "Data Anggota " . $request->nama;
        History::create($histori);

        $notification = array(
            'message' => 'Data Anggota "' . $request->nama . '" berhasil diubah.',
            'alert-type' => 'success'
        );

        return redirect()->route('admin.anggota.index')->with($notification);
    }

    public function destroy($id)
    {
        $data = Anggota::findOrFail($id);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Hapus";
        $histori['desc_info'] = "Data Anggota " . $data->nama;
        History::create($histori);

        $notification = array(
            'message' => 'Data Anggota "' . $data->nama . '" berhasil dihapus.',
            'alert-type' => 'error'
        );

        $data->delete();

        return redirect()->route('admin.anggota.index')->with($notification);
    }
}
