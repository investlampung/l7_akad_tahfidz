<?php

namespace App\Exports;

use App\Anggota;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ExportAnggota implements FromCollection, WithHeadings
{

    public function collection()
    {
        return Anggota::all();
    }
    public function headings(): array
    {
        return [
            'Anggota ID',
            'Pekerjaan ID',
            'Nama',
            'No Kapling',
            'Tempat Lahir',
            'Tanggal Lahir',
            'Sistem Bayar',
            'Leader',
            'Alamat',
            'NIK',
            'No Telp',
            'Ket',
        ];
    }
}
