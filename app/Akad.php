<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Akad extends Model
{
    protected $guarded = [];
    protected $dates = ['tanggal', 'created_at'];

    public function anggota()
    {
        return $this->belongsTo('App\Anggota', 'anggota_id');
    }
    public function angsuran()
    {
        return $this->hasMany('App\Angsuran');
    }
}
