<?php

namespace App\Imports;

use App\Akad;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ImportAkad implements ToModel, WithHeadingRow
{
    public function model(array $row)
    {
        $tgl1 = $row['tanggal'];
        if (empty($tgl1)) {
            return new Akad([
                'anggota_id'        => $row['anggota_id'],
                'nomor'             => $row['nomor'],
                'pasal5_a'          => $row['pasal5_a'],
            ]);
        } else {
            return new Akad([
                'anggota_id'        => $row['anggota_id'],
                'nomor'             => $row['nomor'],
                'pasal5_a'          => $row['pasal5_a'],
                'tanggal'           => $this->transformDate($row['tanggal']),
            ]);
        }
    }

    public function transformDate($value, $format = 'Y-m-d')
    {
        try {
            return \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($value));
        } catch (\ErrorException $e) {
            return \Carbon\Carbon::createFromFormat($format, $value);
        }
    }
}
