<?php

namespace App\Imports;

use App\Anggota;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ImportAnggota implements ToModel, WithHeadingRow
{

    public function model(array $row)
    {
        $tgl = $row['tanggal_lahir'];
        if (empty($tgl)) {
            return new Anggota([
                'pekerjaan_id'      => $row['pekerjaan_id'],
                'nama'              => $row['nama'],
                'no_kapling'        => $row['no_kapling'],
                'no_ktp'            => $row['no_ktp'],
                'tempat_lahir'      => $row['tempat_lahir'],
                'jk'                => $row['jk'],
                'alamat'            => $row['alamat'],
                'no_telp'           => $row['no_telp'],
                'sistem_bayar'      => $row['sistem_bayar'],
                'dana'              => $row['dana'],
                'leader'            => $row['leader'],
                'tanam_pohon'       => $row['tanam_pohon'],
                'jadi_shm'          => $row['jadi_shm'],
                'ket'               => $row['ket'],
            ]);
        } else {
            return new Anggota([
                'pekerjaan_id'      => $row['pekerjaan_id'],
                'nama'              => $row['nama'],
                'no_kapling'        => $row['no_kapling'],
                'no_ktp'            => $row['no_ktp'],
                'tempat_lahir'      => $row['tempat_lahir'],
                'tanggal_lahir'     => $this->transformDate($row['tanggal_lahir']),
                'jk'                => $row['jk'],
                'alamat'            => $row['alamat'],
                'no_telp'           => $row['no_telp'],
                'sistem_bayar'      => $row['sistem_bayar'],
                'dana'              => $row['dana'],
                'leader'            => $row['leader'],
                'tanam_pohon'       => $row['tanam_pohon'],
                'jadi_shm'          => $row['jadi_shm'],
                'ket'               => $row['ket'],
            ]);
        }
    }

    public function transformDate($value, $format = 'Y-m-d')
    {
        try {
            return \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($value));
        } catch (\ErrorException $e) {
            return \Carbon\Carbon::createFromFormat($format, $value);
        }
    }
}
