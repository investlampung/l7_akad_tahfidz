<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Anggota extends Model
{
    protected $guarded = [];

    protected $dates = ['tanggal_lahir'];
    public function pekerjaan()
    {
        return $this->belongsTo('App\Pekerjaan', 'pekerjaan_id');
    }

    public function akad()
    {
        return $this->hasMany('App\Akad');
    }
}
